> "Guys, the clock's on the wall." — C. J. Cregg

### Build

```console
tsc
```

or

```console
tsc --watch
```

### TODO

- [x] Group entries by geographical area.
- [ ] Auto-update times every minute?
- [ ] Optional 12-hour mode.
- [x] Auto-detected dark mode.
- [ ] Accept a query param of a UTC timestamp to show it in every other supported time zone.

### License

[0BSD](https://spdx.org/licenses/0BSD.html); go nuts. [Favicon](https://www.svgrepo.com/svg/286727/clock).
