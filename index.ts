// The only time zones that matter, in vague reverse longitudinal order.
const TIME_ZONES: string[] = [
	"America/Edmonton",

	// RuRu
	"Asia/Tokyo",

	// Oceania
	"Asia/Seoul",
	"Pacific/Auckland",
	"Australia/Brisbane",

	// Europe
	"Europe/Tallinn",
	"Europe/Warsaw",
	"Europe/London",

	// Universal
	"UTC",

	// US (suck it, Alaska and Hawaii)
	"America/New_York",
	"America/Chicago",
	"America/Los_Angeles",
];

// Oh no, you idiot, you made the current date const! Now we're frozen in time.
const now = new Date();

// HTML helper
const el = <K extends keyof HTMLElementTagNameMap>(
	tagName: K,
	attrs: Record<string, number | string> = {},
	...content: (string | Node)[]
): HTMLElementTagNameMap[K] => {
	const element = document.createElement(tagName);

	for (const [key, val] of Object.entries(attrs)) {
		element.setAttribute(key, String(val));
	}

	for (const child of content) {
		if (typeof child === "string") {
			element.innerText = child;
		} else {
			element.appendChild(child);
		}
	}
	return element;
};

const table = el("table");

let currentGroup: string | undefined = undefined;
let currentGroupBody: HTMLTableSectionElement | undefined = undefined;

for (const timeZone of TIME_ZONES) {
	// Group Asia and Australia under Pacific.
	let [group, location] = timeZone.split("/");
	if (group === "Australia" || group === "Asia") group = "Pacific";
	location = !location ? group : location.replace(/_/g, " ");
	if (group === "UTC") group = "Europe";
	if (location === "Warsaw") location = "Wrocław"; // shout out to Mac UwU

	// This would have been so much easier if TC39 was standardized some time this century, and I could just use Temporal like a normal person.
	const localTimestamp = now.toLocaleString("en-GB", {
		timeZone,
		weekday: "short",
		month: "short",
		day: "numeric",
		hour: "numeric",
		minute: "2-digit",
	});

	if (!currentGroupBody || group !== currentGroup) {
		currentGroup = group;
		currentGroupBody = el("tbody");
		currentGroupBody.appendChild(el("tr", {}, el("th", { colspan: 2 }, group)));
		table.appendChild(currentGroupBody);
	}

	currentGroupBody.appendChild(
		el("tr", {}, el("td", {}, location), el("td", {}, localTimestamp)),
	);
}

document.getElementsByTagName("body")[0].appendChild(table);
